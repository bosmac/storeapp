package com.bridgephase.store;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class StoreApplication {

	private Inventory inventory;
	private CashRegister cashRegister;

	public StoreApplication(){
		inventory = new Inventory();
		cashRegister = new CashRegister(inventory);
	}
	public List<Product> getProductList(){
		return inventory.list();
	}
	public void updateProductList(InputStream input){
		inventory.replenish(input);
	}
	public void updateProduct(String upc,String name,Double wholesalePrice,Double retailPrice,Integer quantity){
		inventory.updateProduct(upc, name, wholesalePrice, retailPrice, quantity);
	}
	public CashRegister getCashRegister(){
		return this.cashRegister;
	}
	
	public static void main(String args[]) {
		System.out.println( "### Starting Store Application ###\n" );
		StoreApplication storeApp = new StoreApplication();
		InputStream input = null;
		OutputStream outputStream = null;
		try {
			input = inputStreamFromString(
					"upc,name,wholesalePrice,retailPrice,quantity\n" + 
					"A123,Apple,0.50,1.00,100\n" + 
					"B234,Peach,0.35,0.75,200\n" +
					"C123,Milk,2.15,4.50,40" );
			System.out.println( "### Adding to inventory ###" );
			storeApp.updateProductList(input);
			for ( Product product : storeApp.getProductList() ) {
				System.out.println("Product: " + product.toString());
			}
			System.out.println();
			System.out.println( "### starting new transaction ###" );
			storeApp.getCashRegister().beginTransaction();
			storeApp.getCashRegister().scan("A123");
			storeApp.getCashRegister().scan("A123");
			storeApp.getCashRegister().scan("A123");
			storeApp.getCashRegister().scan("C123");
			storeApp.getCashRegister().scan("B234");
			storeApp.getCashRegister().scan("B234");
			storeApp.getCashRegister().scan("B234");
			storeApp.getCashRegister().scan("B234");
			storeApp.getCashRegister().scan("B234");
			System.out.println( "Items purchased: " + storeApp.getCashRegister().getItemsPurchased() );
			System.out.println( "Total amount due: " + storeApp.getCashRegister().getTotal() );
			Double payment = 100.00;
			System.out.println( "Total amount paid: " + payment );
			System.out.println( "Total amount change: " + storeApp.getCashRegister().pay(payment) );
			System.out.println();
			System.out.println( "### printing receipt ###" );
			outputStream = new ByteArrayOutputStream();
			storeApp.getCashRegister().printReceipt(outputStream);
			System.out.println( "### List iventory ###" );
			for ( Product product : storeApp.getProductList() ) {
				System.out.println("Product: " + product.toString());
			}
			System.out.println();
			System.out.println( "### Update product ###" );
			storeApp.updateProduct("A123", "Red Delicious Apple", 0.50, 1.25, 500);
			storeApp.updateProduct("C123", "Whole Milk", 2.15, 4.00, 100);
			System.out.println();
			System.out.println( "### List iventory ###" );
			for ( Product product : storeApp.getProductList() ) {
				System.out.println("Product: " + product.toString());
			}
		} catch (Exception e) {
			System.out.println("Something went wrong");
		} finally {
			try {
				input.close();
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static InputStream inputStreamFromString(String value) throws UnsupportedEncodingException {
		return new ByteArrayInputStream(value.getBytes("UTF-8"));
	}
}