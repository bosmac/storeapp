package com.bridgephase.store;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.bridgephase.store.interfaces.IInventory;

public class Inventory implements IInventory {
	
	private List<Product> inventoryList;
	private final String header = "upc,name,wholesalePrice,retailPrice,quantity";
	
	public Inventory(){
		this.inventoryList = new ArrayList<>();
	}
	
	@Override
	public void replenish(InputStream inputStream) {
		ArrayList<String> lines = this.getLinesFromInputStream(inputStream);
		for(String line : lines){
			if( line.equals(header) ){ continue; }
			Product product = this.createProduct(line);
			inventoryList.add(product);
		}
	}

	@Override
	public List<Product> list() {
		final List<Product> productList = this.inventoryList;
		return productList;
	}
	
	public void updateInventory(Map<Product, Integer> itemsPurchased){
		for ( Product purchasedItem : itemsPurchased.keySet() ) {
			for ( Product inventoryItem : this.inventoryList ) {
				if( purchasedItem.getUpc().equals( inventoryItem.getUpc() ) ){
					Integer quantity = inventoryItem.getQuantity();
					Integer amountSold = itemsPurchased.get(purchasedItem);
					Integer newQuantity = quantity - amountSold;
					inventoryItem.setQuantity(newQuantity);
				}
			}
		}
	}
	
	public void updateProduct(String upc,String name,Double wholesalePrice,Double retailPrice,Integer quantity){
		for ( Product inventoryItem : this.inventoryList ) {
			if( inventoryItem.getUpc().equals( upc ) ){
				//This assumes non are null
				//TODO add check for null
				inventoryItem.setName(name);
				inventoryItem.setWholesalePrice(wholesalePrice);
				inventoryItem.setRetailPrice(retailPrice);
				inventoryItem.setQuantity(quantity);
			}
		}
	}
	
	private ArrayList<String> getLinesFromInputStream(InputStream is) {
		BufferedReader bufferedReader = null;
		InputStreamReader inputStreamReader = null;
		ArrayList<String> lines = new ArrayList<String>();
		String line;
		try {
			inputStreamReader = new InputStreamReader(is);
			bufferedReader = new BufferedReader(inputStreamReader);
			while ((line = bufferedReader.readLine()) != null) {
				lines.add(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				inputStreamReader.close();
				bufferedReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return lines;
	}
	
	private Product createProduct(String productString){
		Product product = null;
        String[] values = productString.split(",");
        if(values.length != 5){
        	throw new IllegalArgumentException();
        }
		product = new Product();
		product.setUpc( values[0] );
		product.setName( values[1] );
		product.setWholesalePrice( Double.parseDouble( values[2] ) );
		product.setRetailPrice( Double.parseDouble( values[3] ) );
		product.setQuantity( Integer.parseInt( values[4] ) );
		return product;
	}


}
