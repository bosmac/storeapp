package com.bridgephase.store;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public class CashRegister {

	private Inventory inventory;
	private Map<Product, Integer> itemsPurchased;
	private Integer transactionId;
	private Double amountDue;
	private Double amountPaid;
	private Double amountChange;
	private Integer amountItemsPurchased;
	
	public CashRegister(Inventory inventory){
		this.inventory = inventory;
		transactionId = 0;
	}
	public void beginTransaction(){
		this.transactionId++;
		this.itemsPurchased = new HashMap<>();
		this.amountDue = 0.0;
		this.amountPaid = 0.0;
		this.amountChange = 0.0;
		this.amountItemsPurchased = 0;
	}
	public boolean scan(String upc){
		//search inventory for upc
		for ( Product product : inventory.list() ) {
			//if product upc match scanned upc we have a  match
			if(product.getUpc().equals(upc)){
				//keep track of purchased item & item count
				if(product.getQuantity() == 0){
					throw new IllegalArgumentException("Out of stock!");
				}
				if(itemsPurchased.containsKey(product)){
					Integer quantity = itemsPurchased.get(product);
					quantity++;
					itemsPurchased.put(product, quantity);
					this.amountItemsPurchased++;
					return true;
				}else{
					itemsPurchased.put(product, 1);
					this.amountItemsPurchased++;
					return true;
				}
			}
		}
		return false;
	}
	public double getTotal(){
		for ( Product product : itemsPurchased.keySet() ) {
			Integer quantity = itemsPurchased.get(product);
			amountDue = amountDue + ( product.getRetailPrice() * quantity );
		}
		return this.amountDue;
	}
	public double pay(Double cashAmount){
		this.amountPaid = cashAmount;
		this.amountChange = this.amountPaid - this.amountDue;
		this.updateInventory();
		return this.amountChange;
	}
	public void updateInventory(){
		this.inventory.updateInventory(this.itemsPurchased);
	}
	public void printReceipt(OutputStream os){
		String temp = "";
		for ( Product product : itemsPurchased.keySet() ) {
			String productName = product.getName();
			Integer quantity = itemsPurchased.get(product);
			Double unitPrice = product.getRetailPrice();
			Double totalPrice = unitPrice * quantity;
			temp = temp + productName+" "+quantity+" @ "+"$"+unitPrice+" : "+"$"+totalPrice+"\n";
		}
		String s = 	"\n" + 
					"BridgePhase Convenience Store\n" +
					"-----------------------------\n" +
					"Total Products Bought: " + this.amountItemsPurchased + "\n" +
					"\n" +
					temp +
					"\n" +
					"-----------------------------\n" +
					"Total: "  + this.amountDue + "\n" + 
					"Paid: " + this.amountPaid + "\n" + 
					"Change: " + this.amountChange + "\n" +
					"-----------------------------\n";
		    for (int i = 0; i < s.length(); ++i){
		      try {
				os.write(s.charAt(i));
		      } catch (IOException e) {
					e.printStackTrace();
		      } finally {
				try {
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		      }
		    }
		    System.out.println(os);		    
	}
	public Inventory getInventory() {
		return inventory;
	}
	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}
	public Map<Product, Integer> getItemsPurchased() {
		return itemsPurchased;
	}
	public void setItemsPurchased(Map<Product, Integer> itemsPurchased) {
		this.itemsPurchased = itemsPurchased;
	}
	public Integer getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}
	public Double getAmountDue() {
		return amountDue;
	}
	public void setAmountDue(Double amountDue) {
		this.amountDue = amountDue;
	}
	public Double getAmountPaid() {
		return amountPaid;
	}
	public void setAmountPaid(Double amountPaid) {
		this.amountPaid = amountPaid;
	}
	public Double getAmountChange() {
		return amountChange;
	}
	public void setAmountChange(Double amountChange) {
		this.amountChange = amountChange;
	}
}
