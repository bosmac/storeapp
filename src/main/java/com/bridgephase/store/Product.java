package com.bridgephase.store;

public class Product {
	private String upc;
	private String name;
	private Double wholesalePrice;
	private Double retailPrice;
	private Integer quantity;
	
	public Product(){
	}
	public Product(String upc, String name, Double wholesalePrice, Double retailPrice, Integer quantity) {
		this.upc = upc;
		this.name = name;
		this.wholesalePrice = wholesalePrice;
		this.retailPrice = retailPrice;
		this.quantity = quantity;
	}
	public String getUpc() {
		return upc;
	}
	public void setUpc(String upc) {
		this.upc = upc;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getWholesalePrice() {
		return wholesalePrice;
	}
	public void setWholesalePrice(Double wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}
	public Double getRetailPrice() {
		return retailPrice;
	}
	public void setRetailPrice(Double retailPrice) {
		this.retailPrice = retailPrice;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	@Override
	public String toString(){
		return this.upc + " " + this.name + " " + this.wholesalePrice + " " + this.retailPrice + " " + this.quantity;
	}



}
